module.exports = {
  pathPrefix: `/dialoguesavecchantaldumas`,
  assetPrefix: `https://avatarquebec.org`,
  plugins: [
    "gatsby-plugin-emotion",
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `artist`,
        path: `${__dirname}/src/data/artists`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `archives`,
        path: `${__dirname}/src/data/archives`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `nothingButWater`,
        path: `${__dirname}/src/data/nothingButWater`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `audioWorks`,
        path: `${__dirname}/src/data/audioWorks`,
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [".mdx", ".md"],
        gatsbyRemarkPlugins: [],
        plugins: [],
      },
    },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        // icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: "gatsby-plugin-i18n",
      options: {
        langKeyDefault: "fr",
        useLangKeyLayout: false,
      },
    },
  ],
}
