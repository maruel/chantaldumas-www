---
title: Céline Huyghebaert
locale: fr
---

Céline Huyghebaert construit des œuvres qui oscillent entre les arts visuels et la littérature. Elle utilise le langage comme outil principal pour changer les silences et les oublis en histoires.
