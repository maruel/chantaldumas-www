---
title: Carole Rieussec
locale: fr
---

Carole Rieussec est artiste électro-acoustique et performeuse. Depuis 1986, elle compose avec les bruits, les voix et les rythmiques du monde. Sur scène, elle active des espaces sonores improbables.
