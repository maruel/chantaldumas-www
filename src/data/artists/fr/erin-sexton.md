---
title: Erin Sexton
locale: fr
---

Par son travail artistique, Erin Sexton explore des paradigmes alternatifs, d’étranges topologies, de légères apocalypses et des cosmologies spéculatives. Ses œuvres impliquent l’utilisation d’objets trouvés, de bâches, de cordes, de textiles, de procédés de cristallisation et de radiodiffusion.
