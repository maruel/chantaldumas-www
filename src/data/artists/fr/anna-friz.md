---
title: Anna Friz
locale: fr
---

Depuis 1998, Anna Friz crée principalement des œuvres radiophoniques autoréflexives destinées à la radiodiffusion, à l’installation ou à la performance. La radio y est à la fois la source, le sujet et le support de l’œuvre.
