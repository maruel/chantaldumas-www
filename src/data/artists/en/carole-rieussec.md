---
title: Carole Rieussec
locale: en
---

Carole Rieussec is an electroacoustic artist and performer. Since 1986, she has been composing work with the noises, voices, and rhythms of the world. On the stage,
she creates improbable sonic spaces.
