---
title: Erin Sexton
locale: en
---

Erin Sexton is an artist who explores alternate paradigms, strange topologies, soft apocalypses, and speculative cosmologies. Her work involves found objects, tarpaulin, rope, textiles, crystallization, and radio transmission.
