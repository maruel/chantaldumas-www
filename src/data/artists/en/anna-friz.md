---
title: Anna Friz
locale: en
---

Since 1998, Anna Friz has been creating predominantly self-reflexive radio for broadcast, installation, or performance, where radio is the source, subject, and medium of the work.
