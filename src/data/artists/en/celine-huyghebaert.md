---
title: Céline Huyghebaert
locale: en
---

Céline Huyghebaert makes work that oscillates between visual art and literature. Using language as her primary tool, she tries to rectify history’s silences and omissions.
