/* eslint-disable jsx-a11y/media-has-caption */
// vendors
import React, { createContext, Component } from "react"
import ReactPlayer from "react-player"

const defaultState = {
  playStatus: "STOPPED",
  playlist: [],
  currentAlbum: undefined,
  currentPlaying: 0,
  progress: undefined,
  seeking: false,
  playing: false,
  duration: null,
}

const defaultAction = {
  pause: () => {},
  play: () => {},
  updatePlaylist: () => {},
  changeCurrentPlaying: () => {},
}

const PlayerContext = createContext({ ...defaultState, ...defaultAction })

export class PlayerProvider extends Component {
  constructor(props) {
    super(props)

    this.state = {
      ...defaultState,
    }

    this.audioPlayerRef = React.createRef()

    this.changeCurrentPlaying = this.changeCurrentPlaying.bind(this)
    this.handleEnded = this.handleEnded.bind(this)
    this.handlePaused = this.handlePaused.bind(this)
    this.handlePlaying = this.handlePlaying.bind(this)
    this.handleProgress = this.handleProgress.bind(this)
    this.updatePlaylist = this.updatePlaylist.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
  }

  handleEnded(e) {
    const { playlist, currentPlaying } = this.state

    this.setState({ playStatus: "STOPPED" })

    if (playlist.length > currentPlaying + 1) {
      this.changeCurrentPlaying(currentPlaying + 1)

      return
    }

    this.setState({ ...defaultState })
  }

  handlePaused(e) {
    this.setState({ playStatus: "PAUSED" })
  }

  handlePlaying(e) {
    this.setState({ playStatus: "PLAYING", seeking: false })
  }

  handleProgress(e) {
    this.setState({ progress: e.played })
  }

  cleanPlaylist() {
    this.updatePlaylist([], 0)
  }

  async changeCurrentPlaying(currentPlaying) {
    this.pause()

    await this.setState({
      currentPlaying,
      playStatus: "PLAYING",
      progress: "undefined",
    })

    this.play()
  }

  async updatePlaylist(currentAlbum, playlist = [], currentPlaying = 0) {
    await this.setState({
      playlist,
      currentPlaying,
      currentAlbum,
      playStatus: "PLAYING",
      progress: "undefined",
      seeking: true,
    })

    this.play()
  }

  play() {
    this.setState({ playing: true })
  }

  pause() {
    this.setState({ playing: false })
  }

  render() {
    const { children } = this.props
    const {
      currentAlbum,
      currentPlaying,
      playlist,
      playStatus,
      progress,
      seeking,
      playing,
    } = this.state

    // TODO: Sort and filter playlist
    const sources = playlist[currentPlaying] || []

    return (
      <PlayerContext.Provider
        value={{
          changeCurrentPlaying: this.changeCurrentPlaying,
          pause: this.pause,
          play: this.play,
          updatePlaylist: this.updatePlaylist,
          currentAlbum,
          currentPlaying,
          playStatus,
          progress,
          seeking,
        }}
      >
        <>
          {children}

          <ReactPlayer
            style={{ display: "none" }}
            ref={this.audioPlayerRef}
            playing={playing}
            controls={false}
            onEnded={this.handleEnded}
            onPause={this.handlePaused}
            onPlay={this.handlePlaying}
            onProgress={this.handleProgress}
            url={sources.map(({ src, type }) => ({
              src,
              type,
            }))}
          />
        </>
      </PlayerContext.Provider>
    )
  }
}

export default PlayerContext
